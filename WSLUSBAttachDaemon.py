#!/usr/bin/python3.8 -u

# ----------------------------------------------------------------------------------------------------------------------
#
# WSLUSBAttachDaemon.py
# Author: Mike Schoonover
# Date: 5/26/22
#
# Function:
#
# This program runs in a PowerShell window opened as Administrator. It will continuously monitor the list of
# connected USB devices for any unattached device reporting any of the phrases in USB_TARGET_PHRASES. It will then
# attach any such devices to the default WSL Linux distro for use by programs in that distro.
#
# The USB_TARGET_PHRASES list usually contains the device VID (Vendor ID) numbers (such as '03e7' in the example below)
# or the DEVICE names (such as 'Movidius MyriadX' in the example below). Sometimes, the DEVICE name for the same device
# will change during use (some devices appear first with one name and then reappear as a different name after setup),
# so it is often safer to use the VID.
#
# For devices which do change names, both names can be placed in USB_TARGET_PHRASES so that either name will match
# and cause the associated device to be attached. This method can be used in cases where using the VID number is not
# feasible for some reason.
#
# Customization:
#
# Search for 'USB_TARGET_PHRASES =' in this file and modify to meet your specific needs.
#
# You can get the vendor ID (VID) of a device by plugging it in and executing 'usbipd wsl list<enter>' in a PowerShell
# window.
#
# Note that the colon (:) is appended to each VID in case that same number phrase happens to appear elsewhere in the
# list as a DEVICE name or such. Probably not necessary, but helps to limit the match to actual VIDs which are always
# followed by a colon in the list.
#
# Examples:
#
#   USB_TARGET_PHRASES = ["03e7:"]     # Vendor ID for Luxonis, maker of Oak-D-Lite AI camera systems
#
#   USB_TARGET_PHRASES = ["03e7:", "8087:"]     # Vendor IDs for Luxonis and Intel
#
#   USB_TARGET_PHRASES = ["Movidius MyriadX", "Luxonis Device"] # Oak-D-Lite device names before and after starting
#       (note that using "03e7:" is an alternate way of accomplishing this for Oak-D-Lite devices by Luxonis)
#
# Usage:
#
# Start WSL in Windows.
#
# Open a separate PowerShell Window as Administrator...keep this window and WSL open at all times.
# execute:
# >cd <folder containing WSLUSBAttachDaemon.py>
# >python WSLUSBAttachDaemon.py
#
# use ctrl-C to stop the Daemon
#
# The PowerShell Window should be left open with this program running. It will attach any USB devices which appear so
# that the device can be unplugged/replugged without having to attach manually. If a device disappears and reappears
# as a different device after it is setup, this daemon will automatically attach the new device.
#
# Dependencies:
#
# install Python for Windows
# install WSL USB support: https://docs.microsoft.com/en-us/windows/wsl/connect-usb
#
# Reference:
#
# https://github.com/dorssel/usbipd-win/wiki/WSL-support
#
# Details:
#
# The program invokes the WSL command 'usbipd wsl list' to retrieve the list of connected USB devices. Below is an
# example of the list.
#
# > usbipd wsl list<enter>
#
#   BUSID  VID:PID    DEVICE                                                        STATE
#   3-4    05e3:0749  USB Mass Storage Device                                       Not attached
#   4-1    03e7:2485  Movidius MyriadX                                              Not attached
#   4-3    046d:c52f  USB Input Device                                              Not attached
#
# The program then invokes the WSL command 'usbipd wsl attach --busid ?-??' to attach the first device in the list
# which has STATE 'Not attached'. In the example above, the following would be executed:
#
# > usbipd wsl attach --busid 4-1
#
# If there are multiple devices which meet the criteria, all of those devices will be attached to the default Linux
# distro...one for each time through this program's main loop.
#
# ----------------------------------------------------------------------------------------------------------------------
#
# Explanation for the -u option: !/usr/bin/python -u
#
# Python is optimised for reading in and printing out lots of data. One of these optimisation is that the standard
# input and output of the Python interpreter are buffered. That means that whenever a program tries to use one of those
# streams, the interpreted will block up the usage into large chunks and then send the chunks all in one go. This is
# faster than sending each individual read/write through separately, but obviously has the disadvantage that data can
# get 'stopped up' in the middle.
#
# The -u flag turns off this behaviour.
#
# ----------------------------------------------------------------------------------------------------------------------

import time
import os

from typing import List  # also available: Set, Dict, Tuple, Optional, Final, Tuple

# --------------------------------------------------------------------------------------------------
# main
#

USB_TARGET_PHRASES: List[str] = ["03e7:"]     # Vendor ID for Luxonis, maker of Oak-D-Lite AI camera systems

# alternate method of detecting Oak-D-Lite camera using DEVICE name
# Oak-D-Lite device names before and after starting the device:
#   USB_TARGET_PHRASES: List[str] = ["Movidius MyriadX", "Luxonis Device"]

# this version also attaches mass storage devices -- not sure if this is useful
# USB_TARGET_PHRASES: List[str] = ["03e7:", "USB Mass Storage Device"]

while True:

    stream = os.popen('usbipd wsl list')
    output: str = stream.read()
    # print(output)

    deviceList: List[str] = output.splitlines()

    deviceEntry: str = ""

    for deviceEntry in deviceList:
        print(deviceEntry)

    print("")
    print("Unattached eligible devices:")
    print("")

    for deviceEntry in deviceList:

        if deviceEntry.find("Not attached") != -1:

            targetPhrase: str = ""
            for targetPhrase in USB_TARGET_PHRASES:

                if deviceEntry.find(targetPhrase) != -1:

                    print(deviceEntry)
                    deviceEntrySplit: List[str] = deviceEntry.split(' ')
                    busID: str = deviceEntrySplit[0]
                    print("")
                    print("attaching BUSID " + busID + "...")
                    print("")

                    stream = os.popen('usbipd wsl attach --busid ' + busID)
                    output: str = stream.read()
                    print(output)
                    print("")

    time.sleep(0.3)

# end of main
# --------------------------------------------------------------------------------------------------
# --------------------------------------------------------------------------------------------------
