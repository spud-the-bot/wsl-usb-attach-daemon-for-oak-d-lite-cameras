This collection of scripts enables the use of the Oak D-Lite camera in Windows WSL. Other Oak cameras have not been tested but probably work as well.

USB devices are often used in WSL by installing usbipd-win. The devices are then attached manually in a Windows PowerShell to the currently open WSL Linux instance.

The problem is that when the Oak D-Lite cameras are initialized by whatever software is using them, the cameras disappear and reappear as a different device. This is probably due to switching over to a high-speed connection. This breaks the attachment which was established manually in the PowerShell instance. It is difficult to manually attach the newly appearing device in time for the software to recognize it.

The solution is to keep a PowerShell tab constantly open and running the WSLUSBAttachDaemon.py script from this repository. That script continuously monitors all USB devices and attaches any that appear with the Oak vendor ID to the currently open Linux instance. Thus it will attach the camera in its initial state and then reattach it as its new state when it reappears after running the camera software.

On the Linux side, a script is used to start the desired camera software. That script also runs startUDEVD.sh which will start the UDEVD service if it is not running. For some reason, it often seems to be stopped. The script will prompt to unplug the USB device (the camera) and then to plug it back in. This may not be necessary in later versions of WSL.


**Instructions for Installation**

_Install usbipd-win_

    https://github.com/dorssel/usbipd-win

_Add Camera Vendor ID to the UDEV Rules_

Copy udevd rule file from this repository:

    /etc/udev/rules.d/80-movidius.rules

to

    /etc/udev/rules.d/80-movidius.rules

in your Windows WSL folders. (see below for the easy way to do that)

Copy these files from this repository to your C:\Users\[your user name] folder:

    C:\users\[your user name]\wslusb.cmd
    C:\users\[your user name]\WSLUSBAttachDaemon.py

These files will now be visible using the 'dir' command when you open a PowerShell.

_Install Python on Your Windows Machine_

    https://learn.microsoft.com/en-us/windows/python/beginners#install-python


_Create a Script to Run the Camera Software in a WSL Linux Tab_

This is optional and only serves to make sure the udevd service is running. This may not be an issue in later versions of WSL. Also, you may choose to start the service manually.

For an example, in this repository see:

    start depthAI demo.sh


**Instructions for Use**

_Start the Auto Attach Daemon_

Open Windows PowerShell as a tab in Windows Terminal. You may need to do this as admin if you encounter permission issues.

Start the WSL USB Attach Daemon using the convenience script:

    ./wslusb.cm

The daemon will continuously list the available USB devices. Leave this PowerShell tab open and running the script.

_Start the Software Which Uses the Oak Camera_

Open a WSL Linux tab in Windows Terminal.

Run the script which starts the camara software:

    source 'start ASL demo.sh'


**Easy Way to Copy Files to a WSL Folder**

use 'cd' to change to the desired folder in WSL

type:

    explorer.exe .<enter>

Note the ' .' at the end...that forces Explorer to open in the current directory.

Now you can simply drag files into the folder.
