#!/bin/bash

# This script activates the Python virtual environment set up
# for the depthai_demo program.

echo
echo Invoking Python Virtual Environment for Oak depthai_demo.

echo WARNING: this script must be called by: source virtual.sh
echo          so that it acts on  the current shell!
echo

source python/depthai_p3venv/bin/activate