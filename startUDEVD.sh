#!/bin/bash

#
# systemd-udevd Starter 
#
# Company:	Folsom Institute of Artificial Intelligence and Robotics
# Author:	Mike Schoonover
# Date:		5/29/2022
#

#
# This program will start systemd-udevd daemon if it is not
# running and instruct the user to unplug the USB device. This daemon
# controls access to devices such as USB and must be running.
# After starting, the affected USB devices must be unplugged/plugged.
#
# On normal Linux systems, this daemon is started automatically. In
# WSL, it is often not started since systemd does not function in WSL
# as of 5/27/2022.
#


echo
echo This script must be run using the 'source' command!
echo
echo source startUDEVD.sh
echo

SERVICE="systemd-udevd"

if pgrep -x "$SERVICE" >/dev/null
then
    echo
    echo "$SERVICE is running..."
    echo
else

    read -p "Unplug the USB device...press [Enter] key to continue..."

    echo

    echo "Starting the systemd-udevd daemon..."

    # this daemon must be running to access the USB device!
    sudo /lib/systemd/systemd-udevd --daemon

    echo

    read -p "Plug in USB device...press [Enter] key to continue..."

fi
